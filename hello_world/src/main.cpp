#include	"main.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int main()
{
    // Создаем сцену, загружая модель чайника собственного формата VSG
    auto scene = vsg::read_cast<vsg::Node>("../data/models/teapot.vsgt");

    // Создаем вьювер
    auto viewer = vsg::Viewer::create();

    // Создаем объект, хранящий параметры окна приложения
    auto windowTraits = vsg::WindowTraits::create();
    windowTraits->width = 1024;
    windowTraits->height = 768;
    // Создаем окно приложения
    auto window = vsg::Window::create(windowTraits);
    // СВязываем вьювер с коном приложения
    viewer->addWindow(window);

    double radius = 1.0;
    double nearFarRatio = 0.00001;

    // Задаем начальное направление камеры
    auto lookAt = vsg::LookAt::create(vsg::dvec3(0.0, -radius * 4.5, 0.0),
                                      vsg::dvec3(0.0, 0.0, 0.0),
                                      vsg::dvec3(0.0, 0.0, 1.0));

    // Вычисляем параметры и создаем матрицу перспективной проекции
    double aspectRatio = static_cast<double>(window->extent2D().width) /
                         static_cast<double>(window->extent2D().height);

    auto perspective = vsg::Perspective::create(30.0,
                                                aspectRatio,
                                                nearFarRatio * radius,
                                                radius * 100.0);

    // Создаем камеру
    auto camera = vsg::Camera::create(perspective,
                                      lookAt,
                                      vsg::ViewportState::create(window->extent2D()));

    // Задаем обработчик закрытия окна
    viewer->addEventHandler(vsg::CloseHandler::create(viewer));
    // Задаем обработчик манипулятора камеры
    viewer->addEventHandler(vsg::Trackball::create(camera));

    // Создаем граф команд Vulkan для рендеринга сцены в заданое окно
    // определенной ранее камерой
    auto commandGraph = vsg::createCommandGraphForView(window, camera, scene);
    // Связываем граф команд с вьювером
    viewer->assignRecordAndSubmitTaskAndPresentation({commandGraph});

    // Компилируем объекты Vulkan и передаем данные для рендеринга сцены
    viewer->compile();

    while (viewer->advanceToNextFrame()) // пока возможен переход к новому кадру
    {
        // Передаем события в обработчики, назначенные вьюверу
        viewer->handleEvents();
        // Обновляем граф сцены (добавление/удаление объектов, например)
        viewer->update();
        // Записываем команды в граф сцены и послылаем их в очередь Vulkan
        viewer->recordAndSubmit();
        // Ждем окончания рендеринга и передаем готовый кадр в буфер окна
        viewer->present();
    }

    return 0;
}
